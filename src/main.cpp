/*
 * Sample program for DocodeModem　mini
 * Copyright (c) 2020 Circuit Desgin,Inc
 * Released under the MIT license
 *
 * モデムとデバッグ用シリアルをつなげて直接操作できるようにする
 *
 */

#include <docodemo.h>

DOCODEMO Dm = DOCODEMO();

TaskHandle_t Handle_main_task;
static void main_task(void *pvParameters)
{
  SerialDebug.println("Main start");
  Dm.begin();

  bool USE_INSIDE_UART = true;
  if (USE_INSIDE_UART)
  {
    Dm.ModemPowerCtrl(ON);
    delay(150); // Wait for the boot time of the modem
    UartModem.begin(19200);
  }
  else
  {
    Dm.exPowerCtrl(ON);
#ifdef MINI_V2
    Dm.exUartPowerCtrl(ON);
#else
    Dm.exComPowerCtrl(ON);
#endif
    UartExternal.begin(19200);
  }

  Dm.LedCtrl(GREEN_LED, ON);
  Dm.LedCtrl(RED_LED, OFF);

  while (1)
  {
    if (USE_INSIDE_UART)
    {
      if (SerialDebug.available())
      {
        uint8_t data = SerialDebug.read();
        UartModem.write(data);
        //SerialDebug.write(data); // echo back
      }

      if (UartModem.available())
      {
        SerialDebug.write(UartModem.read());
      }
    }
    else
    {
      if (SerialDebug.available())
      {
        uint8_t data = SerialDebug.read();
        UartExternal.write(data);
        //SerialDebug.write(data); // echo back
      }

      if (UartExternal.available())
      {
        SerialDebug.write(UartExternal.read());
      }
    }

    vTaskDelay(1);
  }

  vTaskDelete(NULL);
}

void setup()
{

  SerialDebug.begin(115200);
  while (!SerialDebug)
    ;

  vSetErrorSerial(&SerialDebug);

  xTaskCreate(main_task, "main_task", 1024, NULL, tskIDLE_PRIORITY + 1, &Handle_main_task);

  vTaskStartScheduler();

  // error scheduler failed to start
  // should never get here
  while (1)
  {
    SerialDebug.println("Scheduler Failed! \n");
    delay(1000);
  }
}

void loop()
{
  //never come here
}
